module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ['plugin:vue/essential', 'eslint:recommended', '@vue/prettier'],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-unused-vars': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'prettier/prettier': [
      'error',
      {
        arrowParens: 'avoid',
        htmlWhitespaceSensitivity: 'strict',
        jsxBracketSameLine: true,
        printWidth: 80,
        semi: false,
        singleQuote: true,
        trailingComma: 'none',
        endOfLine: 'auto'
      }
    ]
  }
}
